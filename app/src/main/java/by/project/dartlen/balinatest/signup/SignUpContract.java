package by.project.dartlen.balinatest.signup;

import android.text.Editable;

import java.util.ArrayList;

import by.project.dartlen.balinatest.base.BasePresenter;
import by.project.dartlen.balinatest.base.BaseView;

public interface SignUpContract{
    interface View extends BaseView<Presenter> {
        void showErrorMessagePassword(String message);
        void hideErrorMEssagePassword();

        void showErrorMessageEmail(String message);
        void hideErrorMEssageEmail();
        void showListDomains(ArrayList<String> list);

        void showToast(String text);
    }
    interface Presenter extends BasePresenter {
        void passwordAfterTextChanged(Editable s);
        void passwordBeforeTextChanged(CharSequence s, int start, int count, int after);
        void passwordOnTextChanged(CharSequence s, int start, int before, int count);

        void validateEmail(String email);

        void emailAfterTextChanged(Editable s);
        void emailBeforeTextChanged(CharSequence s, int start, int count, int after);
        void emailOnTextChanged(CharSequence s, int start, int before, int count);

        void onRegistration(String email, String password);

        void dispose();
    }
}
