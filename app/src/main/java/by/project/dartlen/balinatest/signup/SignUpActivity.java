package by.project.dartlen.balinatest.signup;

import androidx.appcompat.app.AppCompatActivity;
import by.project.dartlen.balinatest.R;
import by.project.dartlen.balinatest.network.ValidationEmail;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import java.util.ArrayList;

import static by.project.dartlen.balinatest.util.Constants.URL.ROOT_URL;


public class SignUpActivity extends AppCompatActivity implements SignUpContract.View {

    private SignUpContract.Presenter presenter;
    private Unregistrar mUnregistrar;
    private TextInputLayout textInputLayoutPassword;
    private ValidationEmail validationEmail;
    private TextInputLayout textInputLayoutEmail;
    private AutoCompleteTextView login;
    private TextInputEditText password;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        validationEmail = provideApiInterface(provideRetrofit(provideOkhttpClient()));
        presenter = new SignUpPresenter( validationEmail,this);

        Button button = findViewById(R.id.button);

        ImageView image = findViewById(R.id.imageView);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);

        textInputLayoutEmail = findViewById(R.id.textInputLayout);
        textInputLayoutPassword = findViewById(R.id.textInputLayout2);

        password.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus){
                textInputLayoutPassword.setHintEnabled(true);
                textInputLayoutPassword.setHint(getResources().getString(R.string.password));
                password.setHint(getResources().getString(R.string.message_password));
            }else {
                if(password.getText().toString().length()>0)
                    textInputLayoutPassword.setHintEnabled(true);
                else
                    textInputLayoutPassword.setHintEnabled(false);
                textInputLayoutPassword.setHint(getResources().getString(R.string.password));
                password.setHint(getResources().getString(R.string.message_password));
            }
        });

        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(this, isOpen -> {
            if(isOpen) {
                image.setVisibility(View.GONE);
            }
            else{
                image.setVisibility(View.VISIBLE);
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                presenter.passwordAfterTextChanged(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                presenter.passwordBeforeTextChanged(s,start,count,after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                presenter.passwordOnTextChanged(s,start,before,count);
            }
        });

        login.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.emailBeforeTextChanged(charSequence, i, i1, i2);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.emailOnTextChanged(charSequence, i, i1, i2);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.emailAfterTextChanged(editable);
            }
        });

        button.setOnClickListener(v->{
                presenter.onRegistration(login.getText().toString(),
                    password.getText().toString()
                    );
        });

    }

    public OkHttpClient provideOkhttpClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }

    public Retrofit provideRetrofit(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl(ROOT_URL)
                .build();
    }

    public ValidationEmail provideApiInterface(Retrofit retrofit){
        return retrofit.create(ValidationEmail.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnregistrar.unregister();
    }

    @Override
    public void showErrorMessagePassword(String message) {
        textInputLayoutPassword.setError(message);
    }

    @Override
    public void hideErrorMEssagePassword() {
        textInputLayoutPassword.setError(null);
    }

    @Override
    public void showErrorMessageEmail(String message) {
        textInputLayoutEmail.setError(message);
    }

    @Override
    public void hideErrorMEssageEmail() {
        textInputLayoutEmail.setError(null);
    }

    @Override
    public void showListDomains(ArrayList<String> list) {
        login.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                list));
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this,text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.dispose();
    }
}
