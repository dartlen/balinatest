package by.project.dartlen.balinatest.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Email {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("did_you_mean")
    @Expose
    private String didYouMean;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("domain")
    @Expose
    private String domain;
    @SerializedName("format_valid")
    @Expose
    private Boolean formatValid;
    @SerializedName("mx_found")
    @Expose
    private Boolean mxFound;
    @SerializedName("smtp_check")
    @Expose
    private Boolean smtpCheck;
    @SerializedName("catch_all")
    @Expose
    private Object catchAll;
    @SerializedName("role")
    @Expose
    private Boolean role;
    @SerializedName("disposable")
    @Expose
    private Boolean disposable;
    @SerializedName("free")
    @Expose
    private Boolean free;
    @SerializedName("score")
    @Expose
    private Double score;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDidYouMean() {
        return didYouMean;
    }

    public void setDidYouMean(String didYouMean) {
        this.didYouMean = didYouMean;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean getFormatValid() {
        return formatValid;
    }

    public void setFormatValid(Boolean formatValid) {
        this.formatValid = formatValid;
    }

    public Boolean getMxFound() {
        return mxFound;
    }

    public void setMxFound(Boolean mxFound) {
        this.mxFound = mxFound;
    }

    public Boolean getSmtpCheck() {
        return smtpCheck;
    }

    public void setSmtpCheck(Boolean smtpCheck) {
        this.smtpCheck = smtpCheck;
    }

    public Object getCatchAll() {
        return catchAll;
    }

    public void setCatchAll(Object catchAll) {
        this.catchAll = catchAll;
    }

    public Boolean getRole() {
        return role;
    }

    public void setRole(Boolean role) {
        this.role = role;
    }

    public Boolean getDisposable() {
        return disposable;
    }

    public void setDisposable(Boolean disposable) {
        this.disposable = disposable;
    }

    public Boolean getFree() {
        return free;
    }

    public void setFree(Boolean free) {
        this.free = free;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

}