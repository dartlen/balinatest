package by.project.dartlen.balinatest.network;

import io.reactivex.Maybe;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ValidationEmail {
    @GET("/check")
    Call<Email> validate2(@Query("access_key") String api_key, @Query("email") String email);

    @GET("/check")
    Maybe<Response<Email>> validate(@Query("access_key") String api_key, @Query("email") String email);
//key = b554473f6135bd9044dc44c168cacb4b
}
