package by.project.dartlen.balinatest.signup;

import android.text.Editable;
import android.util.Patterns;

import java.util.ArrayList;

import by.project.dartlen.balinatest.network.ValidationEmail;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static by.project.dartlen.balinatest.util.Constants.VALUES.API_KEY;

public class SignUpPresenter implements SignUpContract.Presenter {

    private SignUpContract.View view;
    private ValidationEmail retrofit;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    String[] domains = new String[]{
            "aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com",
            "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com",
            "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk",
    };

    public SignUpPresenter(ValidationEmail retrofit,SignUpContract.View view) {
        this.retrofit = retrofit;
        this.view = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void passwordAfterTextChanged(Editable s) {
        if(s.toString().length()==0)
            view.hideErrorMEssagePassword();
    }

    @Override
    public void passwordBeforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void passwordOnTextChanged(CharSequence s, int start, int before, int count) {
        if(s.toString().length()<5)
            view.showErrorMessagePassword("Пароль должен состоять минимум из 6 символов");
        else if(s.toString().length()>25)
            view.showErrorMessagePassword("Пароль должен состоять максимум из 25 символов");
        else if(s.toString().length()>5 && s.toString().length()<25)
            view.hideErrorMEssagePassword();
    }

    @Override
    public void validateEmail(String email) {
        Disposable disposable = retrofit.validate(API_KEY,email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        emailResponse -> {

                            if (emailResponse.body().getDidYouMean() != null ) {
                                if(!emailResponse.body().getDidYouMean().equals(""))
                                    view.showErrorMessageEmail("Может быть вы имели ввиду - " + emailResponse.body().getDidYouMean());
                            }
                        },
                        throwable -> {

                        }
                );
        mCompositeDisposable.add(disposable);

    }

    @Override
    public void emailAfterTextChanged(Editable s) {
        if (Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()){
            view.hideErrorMEssageEmail();

        } else {
            view.showErrorMessageEmail("не корректный email");
            validateEmail(s.toString());
        }

        ArrayList<String> list=null;
        String str = s.toString();

        int atSignPosition = str.indexOf('@');
        if (   atSignPosition > 1
                && atSignPosition < (str.length()+1))
        {
            String lookup = str.substring(atSignPosition+1);
            String prefix = str.substring(0,atSignPosition+1);
            list = new ArrayList<String>();
            for (String domain : domains){
                String option = prefix+domain;
                if (option.startsWith(s.toString())) {
                    list.add(option);
                }
            }

        }
        if(list!=null){
            view.showListDomains(list);
        }
        if(s.toString().length()==0)
            view.hideErrorMEssageEmail();
    }

    @Override
    public void emailBeforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void emailOnTextChanged(CharSequence s, int start, int before, int count) {
        if (Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()){
            view.hideErrorMEssageEmail();
            validateEmail(s.toString());
        } else {
            view.showErrorMessageEmail("не корректный email");

        }
    }

    @Override
    public void onRegistration(String email, String password) {
        if (email.length()>0 && password.length()>0 ){
            view.showToast("Ок");
        }else if(email.length()==0 && password.length()==0){
            view.showToast("Данные не введены");
        }else if(email.length()>0 && password.length()==0){
            view.showToast("Пароль не введен");
        }else if(email.length()==0 && password.length()>0){
            view.showToast("Email не введен");
        }

    }

    @Override
    public void dispose() {
        mCompositeDisposable.dispose();
    }
}
